#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <memory>

#include "OpenGL.hpp"
#include <SDL2/SDL.h>

#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/matrix_inverse.hpp"

#include "HelloWorldScene.hpp"

using namespace std;
using namespace glm;
using namespace moodycamel;

#define INDEX_OFFSET ((i + 1) == stacks) ? 1 : j

#define GL_GET_ERROR { \
	GLenum err; \
	if ((err = glGetError()) != GL_NO_ERROR) \
		cerr << "OpenGL Error at " << __FILE__ << ":" << __LINE__ << ": " << gluErrorString(err) << endl; \
}

namespace {
    void errorCallback(GLenum source, GLenum type, GLuint id, 
            GLenum severity, GLsizei length, const char* message, void* userParam) {
        printf("%s\n", message);
    }
}

HelloWorldScene::HelloWorldScene() : eventQueue(10) {
    glClearColor(0, 0, 0, 0);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glEnable(GL_CULL_FACE);

    if (GLEW_KHR_debug) {
        glDebugMessageCallback(errorCallback, NULL);
        glEnable(GL_DEBUG_OUTPUT);
    }

    initShader();
    generateSphere(10);

    globalMatrices = new UniformBufferObject(NULL, sizeof(mat4) * 2, 
        GL_STREAM_DRAW);

    auto globalMatricesBlockIndex = shader->getUniformBlock("GlobalMatrices");
    shader->bindUniformBlock(globalMatricesBlockIndex, 0, globalMatrices->getBuffer(),
        sizeof(mat4) * 2);
}

HelloWorldScene::~HelloWorldScene() {
}

void HelloWorldScene::initShader() {
    //shaders are version 100 for webgl compatibility
    shader = new Shader();
    shader->initWithFiles("NormalDiffuse.vert", "NormalDiffuse.frag")
            ->bind();
}

void HelloWorldScene::generateSphere(const uint32_t lod) {
    const float radius = 1.0f;
    const uint32_t slices = lod;
    const uint32_t stacks = slices / 2 + 1;

    uint32_t i, j; 
    float u, v;
    vector<vec3> vertices, normals;
    vector<uint16_t> indices;

    vertices.reserve(2 + (stacks - 1) * slices);
    normals.reserve(2 + (stacks - 1) * slices);
    indices.reserve(2 + slices + 2 * (stacks - 1) + 2 * (stacks - 1) * slices);

    normals.push_back(vec3(0.0f, 0.0f, 1.0f));
    vertices.push_back(vec3(0.0f, 0.0f, radius));

    for (i = 0; i < slices; ++i) {
        indices.push_back(0);
        indices.push_back(i + 1);
    }
    
    indices.push_back(0);
    indices.push_back(1);

    for (i = 1; i < stacks; ++i) {
        v = i / static_cast<float>(stacks);
        for (j = 0; j < slices; ++j) {
            u = j / static_cast<float>(slices);

            const float theta = u * 2.0f * M_PI;
            const float phi = v * M_PI;

            vec3 vert, norm;

            norm.x = cos(theta) * sin(phi);
            norm.y = sin(theta) * sin(phi);
            norm.z = cos(phi);

            vert.x = radius * norm.x;
            vert.y = radius * norm.y;
            vert.z = radius * norm.z;

            normals.push_back(norm);
            vertices.push_back(vert);

            indices.push_back((i - 1) * slices + (j + 1));
            indices.push_back(i * slices + (INDEX_OFFSET + 1));
        }

        indices.push_back((i - 1) * slices + 1);
        indices.push_back(i * slices + 1);
    }

    normals.push_back(vec3(0.0f, 0.0f, -1.0f));
    vertices.push_back(vec3(0.0f, 0.0f, -radius));

    vao = new VertexArrayObject();

    this->vertices = new VertexBufferObject(vertices);
    shader->attribPointer("position", this->vertices);
    
    this->normals = new VertexBufferObject(normals);
    shader->attribPointer("normal", this->normals);
    
    this->indices = new IndexBufferObject(indices);
    
    vao->unbind();
}

void HelloWorldScene::reshape(const int32_t w, const int32_t h) {
    glViewport(0, 0, w, h);
    const float aspect = w / static_cast<float>(h);
    projectionMatrix = perspective(75.0f, aspect, 0.01f, 100.0f);
    eventQueue.try_enqueue(events::resized);
}

void HelloWorldScene::update(const float dt) {

}

void HelloWorldScene::display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    modelMatrix = viewMatrix = mat4(1.0f);

    viewMatrix = translate(viewMatrix, vec3(0.0f, 0.0f, -5.0f));
    const mat3 normalMatrix = inverseTranspose(mat3(viewMatrix * modelMatrix));

    int32_t event;
    bool success = eventQueue.try_dequeue(event);
    if(success) {
        switch(event) {
            case events::resized:
                globalMatrices->update(value_ptr(projectionMatrix), sizeof(mat4), sizeof(mat4));
                break;
        }
    }

    //should update view and proj matrix in update and reshape respectively 
    //(but cant atm due to threading)
    globalMatrices->update(value_ptr(viewMatrix), 0, sizeof(mat4));
                    
    shader->bind()
        ->mat("modelMatrix", modelMatrix)
        ->mat("normalMatrix", normalMatrix);
        
    vao->bind();
    indices->draw(GL_TRIANGLE_STRIP);
}

void HelloWorldScene::mouseDown(const int32_t button, const int32_t state, const int32_t x, const int32_t y) {}
void HelloWorldScene::mouseMove(const int32_t x, const int32_t y) {}
void HelloWorldScene::keyDown(const uint32_t key, const uint32_t mod) { }
void HelloWorldScene::keyUp(const uint32_t key, const uint32_t mod) {}
