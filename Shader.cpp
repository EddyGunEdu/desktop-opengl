#include "glm/gtc/type_ptr.hpp"
#include <iostream>
#include <SDL2/SDL.h>

#include "OpenGL.hpp"
#include "Shader.hpp"
#include "Loader.hpp"

using namespace std;
using namespace glm;

Shader::Shader(){}

Shader::~Shader() {
    glDeleteProgram(program);
}

Shader*  Shader::initWithStrings(const string& vert, const string& frag) {
    const uint32_t vs = getShader(vert, GL_VERTEX_SHADER);
    const uint32_t fs = getShader(frag, GL_FRAGMENT_SHADER);
    
    program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);
    
    int infologLength = 0, success = 0;
    char *infoLog;

    glGetProgramiv(program, GL_LINK_STATUS, &success);

    if (!success) {
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infologLength);
        if (infologLength > 1) {
            infoLog = new char[infologLength];
            glGetProgramInfoLog(program, infologLength, NULL, infoLog);

            cerr << "Program Info Log: " << infoLog << endl;
            delete infoLog;
        } else {
            cerr << "Program Info Log: <No info log>" << endl;
        }
        
        glDeleteProgram(program);
    }
    
    glDeleteShader(vs);
    glDeleteShader(fs);

    return this;
}

Shader*  Shader::initWithFiles(const string& vert, const string& frag) {
    const string vertString = Loader::loadTextFile(vert);
    const string fragString = Loader::loadTextFile(frag);

    return initWithStrings(vertString, fragString);
}

Shader* Shader::bind() {
    glUseProgram(program);

    return this;
}

void Shader:: unbind() {
    glUseProgram(0);
}

Shader* Shader::attribPointer(const string& name, VertexBufferObject* vbo) {
    int32_t att;
    const auto lookup = attributes.find(name);
    if(lookup == attributes.end()){
		auto loc = glGetAttribLocation(program, name.c_str());
		
        if(loc == -1) {
			cerr << "Could not find attribute " << name << endl;
            return this;
		} else {
            attributes[name] = att = loc;
        }
	} else {
        att = lookup->second;
    }
	
    glEnableVertexAttribArray(att);
	glVertexAttribPointer(att, vbo->getItemSize(), GL_FLOAT, false, 0, 0);
	
    return this;
}

void Shader::clearTextures() {
    for(auto tex = textures.cbegin(); tex != textures.cend(); ++tex){
        glActiveTexture(GL_TEXTURE0 + tex->second);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

Shader* Shader::scalar(const string& name, const int32_t val) {
    glUniform1i(getUniform(name), val);
    return this;
}

Shader* Shader::scalar(const string& name, const float val) {
    glUniform1f(getUniform(name), val);
    return this;
}

Shader* Shader::vec(const string& name, const vec2& val) {
    glUniform2fv(getUniform(name), 1, value_ptr(val));
    return this;
}

Shader* Shader::vec(const string& name, const vec3& val) {
    glUniform3fv(getUniform(name), 1, value_ptr(val));
    return this;
}

Shader* Shader::vec(const string& name, const vec4& val) {
    glUniform4fv(getUniform(name), 1, value_ptr(val));
    return this;
}

Shader* Shader::mat(const string& name, const mat3& val) {
    glUniformMatrix3fv(getUniform(name), 1, false, value_ptr(val));
    return this;
}

Shader* Shader::mat(const string& name, const mat4& val) {
    glUniformMatrix4fv(getUniform(name), 1, false, value_ptr(val));
    return this;
}

Shader* Shader::texture(const string& name, const uint32_t id) {
    if(textures.find(name) == textures.end()){
		int32_t unit = textures.size();
		textures[name] = unit;
		scalar(name, unit);
	}
		
    glActiveTexture(GL_TEXTURE0 + textures[name]);
    glBindTexture(GL_TEXTURE_2D, id);
		
    return this;
}

Shader* Shader::bindUniformBlock(const uint32_t blockIndex, const uint32_t bindingIndex, 
        const uint32_t buffer, const size_t size) {
    glUniformBlockBinding(program, blockIndex, bindingIndex);
    glBindBufferRange(GL_UNIFORM_BUFFER, bindingIndex, buffer, 0, size);

    return this;
}

uint32_t Shader::getShader(const string& src, const uint32_t type) {
    const uint32_t shader = glCreateShader(type);
    const char* c_str = src.c_str();
    glShaderSource(shader, 1, &c_str, NULL);
    glCompileShader(shader);
    
    int infologLength = 0, charsWritten = 0, success = 0;
    char *infoLog;

    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

    if (!success) {
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infologLength);

        auto name = (type == GL_VERTEX_SHADER) ? "vertex" : "fragment";

        if (infologLength > 1) {
            infoLog = new char[infologLength];
            glGetShaderInfoLog(shader, infologLength, &charsWritten, infoLog);
            cerr << name << " shader info:" << endl << infoLog << endl;
            delete infoLog;
        } else {
            cerr << name << " shader info: <No info log>" << endl;
        }
        
        glDeleteShader(shader);
    }
    
    return shader;
}

int32_t Shader::getUniform(const string& name) {
    const auto lookup = uniforms.find(name);
    if(lookup == uniforms.end()){
		auto loc = glGetUniformLocation(program, name.c_str());
		
		if(loc == -1) {
			cerr << "Could not find uniform " << name << endl;
            return 0;
		} else {
            uniforms[name] = loc;
            return loc;
        }
	} else {
        return lookup->second;
    }
}

uint32_t Shader::getUniformBlock(const string& name) {
    return glGetUniformBlockIndex(program, name.c_str());
}

