#include "FramebufferObject.hpp"
#include "OpenGL.hpp"

using namespace std;

FramebufferObject::FramebufferObject(const uint32_t width, const uint32_t height, const bool depth, 
        const uint32_t numAttachments) {
    this->width = width;
    this->height = height;

    glGenFramebuffers(1, &buffer);
    glBindFramebuffer(GL_FRAMEBUFFER, buffer);
		
	if(depth) {
		glGenRenderbuffers(1, &depthbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, 
									width, height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, 
										GL_RENDERBUFFER, depthbuffer);
	}
	
	if(numAttachments > 1) {
		vector<uint32_t> buffers;
		for(uint32_t i = 0; i < numAttachments; ++i) {
			output.push_back(createTexture(GL_RGBA, GL_RGBA, GL_FLOAT));
			buffers.push_back(GL_COLOR_ATTACHMENT0 + i);
			glFramebufferTexture2D(GL_FRAMEBUFFER, buffers[i], 
			    GL_TEXTURE_2D, output[i], 0);
	    }
		glDrawBuffers(buffers.size(), &buffers[0]);
	}else{
		output.push_back(createTexture(GL_RGBA, GL_RGBA, GL_FLOAT)); 
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, 
		    GL_TEXTURE_2D, output[0], 0);
	}
		
	clearMask = depth ? GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT : GL_COLOR_BUFFER_BIT;
	
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

FramebufferObject::~FramebufferObject() {
    glDeleteRenderbuffers(1, &depthbuffer);
}

FramebufferObject& FramebufferObject::bind() {
    glPushAttrib(GL_VIEWPORT_BIT);
    glViewport(0, 0, width, height);
    glBindFramebuffer(GL_FRAMEBUFFER, buffer);
    glClear(clearMask);
    
    return *this;
}

void FramebufferObject::unbind() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glPopAttrib();
}

uint32_t FramebufferObject::createTexture(const uint32_t format, const uint32_t type, 
    	const uint32_t precision) {
    uint32_t texture;
    glGenTextures(1, &texture);
    
	glBindTexture(GL_TEXTURE_2D, texture);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	
	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, type, 
	   				precision, 0);
	return texture;
}
