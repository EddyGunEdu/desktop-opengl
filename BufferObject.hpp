#ifndef _BUFFER_OBJECT_H
#define _BUFFER_OBJECT_H

#include <vector>
#include <cstdint>
#include "OpenGL.hpp"
#include "glm/glm.hpp"

class BufferObject {
public:    
    virtual ~BufferObject();
    
    virtual BufferObject* draw(const uint32_t mode) { return this; }
    
protected:
    template<typename T> BufferObject(const uint32_t type, const std::vector<T>& data, const uint32_t usage){
        numItems = data.size();

        glGenBuffers(1, &buffer);
        glBindBuffer(type, buffer);
        glBufferData(type, numItems * sizeof(T), &data[0], usage);
    }

    uint32_t buffer, numItems;
};

class VertexBufferObject : BufferObject {
public:
    VertexBufferObject(const std::vector<glm::vec3>& data, const uint32_t usage = GL_STATIC_DRAW) :
            BufferObject(GL_ARRAY_BUFFER, data, usage) { 
        itemSize = 3;
    }

    VertexBufferObject(const std::vector<glm::vec2>& data, const uint32_t usage = GL_STATIC_DRAW) :
            BufferObject(GL_ARRAY_BUFFER, data, usage) {
        itemSize = 2; 
    }

    uint32_t getItemSize() { return itemSize; }

    BufferObject* draw(const uint32_t mode);

private:
    uint32_t itemSize;
};

class IndexBufferObject : BufferObject {
public:
    IndexBufferObject(const std::vector<uint16_t>& data, const uint32_t usage = GL_STATIC_DRAW) :
        BufferObject(GL_ELEMENT_ARRAY_BUFFER, data, usage) { }

    BufferObject* draw(const uint32_t mode);

};

class UniformBufferObject {
public:
    UniformBufferObject(void* data, const uint32_t size, 
        const uint32_t usage = GL_STATIC_DRAW);

    UniformBufferObject* update(void* data, const uint32_t offset, const uint32_t size);

    uint32_t getBuffer(){ return buffer; }

private:
    uint32_t buffer;
};

#endif
