#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <memory>

#include "glm/glm.hpp"
#include "readerwriterqueue.h"

#include "Shader.hpp"
#include "BufferObject.hpp"
#include "VertexArrayObject.hpp"

namespace events{
    enum type {
        resized
    };
}

class HelloWorldScene {
public:
    HelloWorldScene();

    ~HelloWorldScene();

    void reshape(const int32_t w, const int32_t h);
    void update(const float dt);
    void display();

    void mouseDown(const int32_t button, const int32_t state, const int32_t x, const int32_t y);
    void mouseMove(const int32_t x, const int32_t y);
    void keyDown(const uint32_t key, const uint32_t mod);
    void keyUp(const uint32_t key, const uint32_t mod);

private:
    void initShader();
    void generateSphere(const uint32_t lod);

    glm::mat4 modelMatrix, viewMatrix, projectionMatrix;
    glm::mat3 normalMatrix;
    VertexArrayObject *vao;
    Shader *shader;
    VertexBufferObject *vertices, *normals;
    IndexBufferObject *indices;
    UniformBufferObject *globalMatrices; 
    moodycamel::ReaderWriterQueue<uint32_t> eventQueue;
};