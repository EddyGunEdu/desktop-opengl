#include <fstream>
#include <png.h>
#include <cstring>

#include "Loader.hpp"

using namespace std;

string Loader::loadTextFile(const string& fileName) {
    ifstream in(fileName, ios::in);
    if (in) {
        string contents;
        in.seekg(0, ios::end);
        contents.resize(in.tellg());
        in.seekg(0, ios::beg);
        in.read(&contents[0], contents.size());
        in.close();
        return contents;
    }

    return "";
}

uint8_t* Loader::loadPngImage(const string& name, uint32_t &width, 
        uint32_t &height, bool &hasAlpha) {
    png_structp png_ptr;
    png_infop info_ptr;
    uint32_t sig_read = 0;
    int32_t color_type, interlace_type;
    FILE* fp;
 
    if ((fp = fopen(name.c_str(), "rb")) == NULL)
        return 0;
 
    if ((png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
                                     NULL, NULL, NULL)) == NULL) {
        fclose(fp);
        return nullptr;
    }
 
    if ((info_ptr = png_create_info_struct(png_ptr)) == NULL) {
        fclose(fp);
        png_destroy_read_struct(&png_ptr, NULL, NULL);
        return 0;
    }
 
    if (setjmp(png_jmpbuf(png_ptr))) {
        fclose(fp);
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        return 0;
    }
 
    png_init_io(png_ptr, fp);
 
    png_set_sig_bytes(png_ptr, sig_read);
 
    png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING | PNG_TRANSFORM_EXPAND, NULL);
 
    int32_t bit_depth;
    png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type,
                 &interlace_type, NULL, NULL);

    switch(color_type){
        case PNG_COLOR_TYPE_RGB_ALPHA:
            hasAlpha = true;
            break;
        case PNG_COLOR_TYPE_RGB:
            hasAlpha = false;
            break;
        default:
            fclose(fp);
            png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    }
 
    int32_t row_bytes = png_get_rowbytes(png_ptr, info_ptr);
    png_byte* outData = new png_byte[row_bytes * height];
 
    png_bytepp row_pointers = png_get_rows(png_ptr, info_ptr);
 
    for (uint32_t i = 0; i < height; i++) {
        memcpy(outData + (row_bytes * (height - 1 - i)), row_pointers[i], row_bytes);
    }
 
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    fclose(fp);
 
    return outData;
}