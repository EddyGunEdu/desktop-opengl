#ifndef _VERTEX_ARRAY_OBJECT_H
#define _VERTEX_ARRAY_OBJECT_H

#include <vector>
#include <cstdint>

#include "glm/glm.hpp"

class VertexArrayObject {
public:
    VertexArrayObject();    
    ~VertexArrayObject();

    VertexArrayObject* bind();
    void unbind();

private:
    uint32_t vao;
};
#endif
