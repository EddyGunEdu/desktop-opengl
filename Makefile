CFLAGS = -std=c++11 
CFLAGS += -g -Wall -Wextra -Wno-unused-parameter
CFLAGS += -O2 -fomit-frame-pointer -ffast-math -flto -march=native
#CLANGHACKS = -D__extern_always_inline=inline
#LDFLAGS = -lSDL2 -lGLEW -lGL -lGLU -lm -lpng -lz
LDFLAGS = -lGLEW -framework OpenGL -framework SDL2 -lpng -lz
CC = clang++
OBJECTS = Loader.o VertexArrayObject.o BufferObject.o Shader.o \
	FramebufferObject.o HelloWorldScene.cpp HelloWorld.o

all: HelloWorld run
	
HelloWorld: $(OBJECTS)
	$(CC) $(CFLAGS) $(CLANGHACKS) $(OBJECTS) -o $@ $(LDFLAGS)

%.o : %.cpp
	$(CC) $(CFLAGS) $(CLANGHACKS) -c -o $@ $<
	
run:
	./HelloWorld
	
clean:
	rm -f *.o *~
