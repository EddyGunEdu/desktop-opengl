#version 330 core
//precision mediump float;

layout(std140) uniform GlobalMatrices {
	mat4 viewMatrix, projectionMatrix;
};

in vec3 position, normal;
in vec2 uv;

uniform mat4 modelMatrix;
uniform mat3 normalMatrix;

out Data {
	vec3 normal, position;
	vec2 uv;
} dataOut;

void main(){
	dataOut.position = (viewMatrix * modelMatrix * vec4(position, 1.0)).xyz;
	dataOut.normal = normalMatrix * normal;
	dataOut.uv = uv;
	
	gl_Position =  projectionMatrix * vec4(dataOut.position, 1.0);
}