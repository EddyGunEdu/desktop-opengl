#include "VertexArrayObject.hpp"
#include "OpenGL.hpp"
#include <iostream>

using namespace std;
using namespace glm;

VertexArrayObject::VertexArrayObject(){
    glGenVertexArrays(1, &vao);
    bind();
}

VertexArrayObject::~VertexArrayObject() {
    glDeleteVertexArrays(1, &vao);
}

VertexArrayObject* VertexArrayObject::bind() {
    glBindVertexArray(vao);

    return this;
}

void VertexArrayObject::unbind() {
    glBindVertexArray(0);
}