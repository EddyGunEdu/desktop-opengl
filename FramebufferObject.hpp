#ifndef _FRAMEBUFFER_OBJECT_H
#define _FRAMEBUFFER_OBJECT_H

#include <cstdint>
#include <vector>

class FramebufferObject {
public:
    FramebufferObject(const uint32_t width, const uint32_t height, const bool depth = false, 
        const uint32_t numAttachments = 1);
    ~FramebufferObject();
    
	FramebufferObject& bind();
	void unbind();
		
private:
    uint32_t createTexture(const uint32_t format, const uint32_t type, const uint32_t precision);
    
    uint32_t width, height, buffer, depthbuffer, clearMask;
    std::vector<uint32_t> output;
};

#endif
