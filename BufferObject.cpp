#include "BufferObject.hpp"
#include "OpenGL.hpp"
#include <iostream>

using namespace std;
using namespace glm;

BufferObject::~BufferObject() {
    glDeleteBuffers(1, &buffer);
}

BufferObject* VertexBufferObject::draw(const uint32_t mode) {
    glDrawArrays(mode, 0, numItems);

    return this;
}

BufferObject* IndexBufferObject::draw(const uint32_t mode) {
    glDrawElements(mode, numItems, GL_UNSIGNED_SHORT, 0);
    
    return this;
}

UniformBufferObject::UniformBufferObject(void* data, const uint32_t size, 
        const uint32_t usage) {    
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_UNIFORM_BUFFER, buffer);
    glBufferData(GL_UNIFORM_BUFFER, size, data, usage);
}

UniformBufferObject* UniformBufferObject::update(void* data, const uint32_t offset, 
    const uint32_t size) {
    glBindBuffer(GL_UNIFORM_BUFFER, buffer);
    glBufferSubData(GL_UNIFORM_BUFFER, offset, size, data);

    return this;
}
