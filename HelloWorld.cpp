#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <memory>
#include <thread>

#include "OpenGL.hpp"
#include <SDL2/SDL.h>

#include "HelloWorldScene.hpp"

using namespace std;

namespace {
    SDL_Window *window;
    SDL_GLContext context;
    HelloWorldScene *helloWorldScene;

    bool quit = false;
    const uint32_t DEFAULT_WIDTH = 800, DEFAULT_HEIGHT = 600;
}

void draw_loop() {
    static uint32_t lastTime = SDL_GetTicks();
    uint32_t thisTime;

    SDL_GL_MakeCurrent(window, context); 

    while(!quit) {
        thisTime = SDL_GetTicks();
        lastTime = thisTime;

         //render results
        helloWorldScene->display();

        //will need to update to take time taken to render frame into account
        SDL_GL_SwapWindow(window);
        SDL_Delay(1.0f/30.0f - static_cast<float>(thisTime - lastTime) / 1000.0f);
    }
}

void update_loop() {
    static uint32_t lastTime = SDL_GetTicks();
    uint32_t thisTime;
    float deltaTime;
    SDL_Event event;

    while(!quit) {
        //calculate elapsed time
        thisTime = SDL_GetTicks();
        deltaTime = static_cast<float>(thisTime - lastTime) / 1000.0f;
        lastTime = thisTime;

        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_WINDOWEVENT:
                    switch (event.window.event) {
                        case SDL_WINDOWEVENT_CLOSE: //exit app
                            quit = true;
                            return;
                            break;
                        case SDL_WINDOWEVENT_RESIZED: //resize screen
                            helloWorldScene->reshape(event.window.data1, event.window.data2);
                            break;
                        default:
                            break;
                    }
                    break;
                case SDL_QUIT:
                    quit = true;
                    return;
                    break;
                case SDL_KEYDOWN:
                    helloWorldScene->keyDown(event.key.keysym.sym, event.key.keysym.mod);
                    break;
                case SDL_KEYUP:
                    helloWorldScene->keyUp(event.key.keysym.sym, event.key.keysym.mod);
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    helloWorldScene->mouseDown(event.button.button, 1, event.motion.x,
                                           event.motion.y);
                    break;
                case SDL_MOUSEBUTTONUP:
                    helloWorldScene->mouseDown(event.button.button, 0, event.motion.x,
                                           event.motion.y);
                    break;
                case SDL_MOUSEMOTION:
                    helloWorldScene->mouseMove(event.motion.x, event.motion.y);
                    break;
                default:
                    break;
            }
        }

        //cpu-side logic
        helloWorldScene->update(deltaTime);

        SDL_Delay(1.0f/30.0f - deltaTime);
    }
}

int32_t main(int32_t argc, char **argv) {
    if(SDL_Init(SDL_INIT_EVERYTHING) != 0){
        cout << "SDL_Init Error: " << SDL_GetError() << endl;
	    return 1;
    }
 
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetSwapInterval(0);

    if((window = SDL_CreateWindow("Hello, World!",
            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
            DEFAULT_WIDTH, DEFAULT_HEIGHT, 
            SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN)) == nullptr){
        cout << "SDL_CreateWindow Error: " << SDL_GetError() << endl;
	    return 1;
    }
        
    if((context = SDL_GL_CreateContext(window)) == nullptr){
        cout << "SDL_GL_CreateContext Error: " << SDL_GetError() << endl;
	    return -1;
    }

    glewExperimental = GL_TRUE;
    glewInit();

    if(GLEW_EXT_direct_state_access) {
        cout << "Direct state access" << endl;
    }

    helloWorldScene = new HelloWorldScene();

    helloWorldScene->reshape(DEFAULT_WIDTH, DEFAULT_HEIGHT);

    auto renderThread = thread(draw_loop);
    update_loop();
    renderThread.join();

    SDL_GL_MakeCurrent(window, context); 
    delete helloWorldScene;
    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(window);
    SDL_Quit();

    //clean up and close
    return 0;
}
