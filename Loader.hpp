#ifndef _UTILS_H
#define _UTILS_H

#include <string>
#include <png.h>

class Loader {
public:
    static std::string loadTextFile(const std::string& fileName);
    uint8_t* loadPngImage(const std::string& name, uint32_t &width, 
        uint32_t &height, bool &hasAlpha) ;
};
#endif
