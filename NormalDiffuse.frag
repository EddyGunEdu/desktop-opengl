#version 330 core
precision mediump float;

//struct Material {

//}

uniform sampler2D tex;

in Data {
	vec3 normal, position;
	vec2 uv;
} dataIn;

out vec4 colour;

void main(){
	vec3 n = normalize(dataIn.normal);
	vec3 e = normalize(vec3(dataIn.position));

	vec3 lightPos = normalize(vec3(1, 0, 0));
	vec3 lightDir = normalize(lightPos - e);
	float intensity = max(dot(n, lightDir), 0.0);

	//texture(Texture, InData.UV).xyz
	colour = vec4(vec3(0.1, 0.1, 0.1) + intensity *  n, 1.0);
}