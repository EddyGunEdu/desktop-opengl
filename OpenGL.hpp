
#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#  include <GL/glew.h>
#  include <OpenGL/gl.h>
#  include <OpenGL/glu.h>
#  include <GLUT/glut.h>
#else
#  ifdef WIN32
#    ifndef WIN32_LEAN_AND_MEAN 
#      define WIN32_LEAN_AND_MEAN
#    endif
#	 include <windows.h>
#  endif
#  include <GL/glew.h>
#  include <GL/gl.h>
#  include <GL/glext.h>
#  include <GL/glu.h>
#endif
