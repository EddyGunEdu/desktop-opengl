#ifndef _SHADER_H
#define _SHADER_H

#include <map>
#include <string>
#include <cstdint>

#include "glm/glm.hpp"

#include "BufferObject.hpp"

class Shader {
public:
    Shader();
    ~Shader();

    Shader* initWithStrings(const std::string& vert, const std::string& frag);
    Shader* initWithFiles(const std::string& vert, const std::string& frag);
    
    Shader* bind();
    void unbind();
    void clearTextures();
    
    Shader* attribPointer(const std::string& name, VertexBufferObject* vbo);
    Shader* scalar(const std::string& name, const int32_t val);
    Shader* scalar(const std::string& name, const float val);
    Shader* vec(const std::string& name, const glm::vec2& val);
    Shader* vec(const std::string& name, const glm::vec3& val);
    Shader* vec(const std::string& name, const glm::vec4& val);
    Shader* mat(const std::string& name, const glm::mat3& val);
    Shader* mat(const std::string& name, const glm::mat4& val);
    Shader* texture(const std::string& name, const uint32_t id);
    Shader* bindUniformBlock(const uint32_t blockIndex, const uint32_t bindingIndex, 
        const uint32_t buffer, const size_t size);

    uint32_t getUniformBlock(const std::string& name);

private:
    uint32_t getShader(const std::string& src, const uint32_t type);
    int32_t getUniform(const std::string& name);
    
    std::map<std::string, int32_t> uniforms, attributes, textures;
    uint32_t program;
};
#endif
